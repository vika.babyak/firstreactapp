import data from './data/data'
import React from 'react'
import ReactDOM from 'react-dom'

console.log(data);

const HelloUser = () => {
  return (
    <div>Hello user!</div>
  )
}

ReactDOM.render(<HelloUser></HelloUser>, document.querySelector('#root'));

